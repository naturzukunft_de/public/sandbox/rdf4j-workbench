# run
docker run -p 8080:8080 --name rdf4j-workbench -v data:/var/rdf4j -v logs:/home/fredy/tomcat/logs registry.gitlab.com/naturzukunft.de/public/fairsync/rdf4j-workbench:master

# connect with bash
docker exec -it rdf4j-workbench bash
